# app/home/views.py

from django.shortcuts import render, get_object_or_404
from django.shortcuts import render
from .models import Cars

from django.utils import timezone
from django.shortcuts import redirect
from django.db.models import Q
from django.db.models import Avg,Count

def dashboard(request):

    """
    initialisation des filtres
    """
    pays=""
    filtre_carburant=""
    filtre_transmission=""
    filtre_annee=""



    """
    Activation  des filtres s'ils sont renseignés
    """
    if request.method == 'POST':
        #suivant le filtre sur les pays
        if request.POST.get('Senegal') is not None:
            pays="Sénégal"
        if request.POST.get('Bénin') is not None:
            pays="Bénin"
        if request.POST.get("Côte d'Ivoire") is not None:
            pays="Côte d'Ivoire"
        if request.POST.get("Togo") is not None:
            pays="Togo"


        #suivant le filtre sur le type de transmission

        if request.POST.get("Manuel") is not None:
            filtre_transmission="Manuel"
        if request.POST.get("Non renseigné") is not None:
            filtre_transmission="Non renseigné"
        if request.POST.get("Automatique") is not None:
            filtre_transmission="Automatique"
        if request.POST.get("close_transmission") is not None:
            filtre_transmission=""


        #suivant le filtre sur le type du carburant

        if request.POST.get("Gasoil") is not None:
            filtre_carburant="Gasoil"
        if request.POST.get("Non renseigné") is not None:
            filtre_carburant="Non renseigné"
        if request.POST.get("Essence") is not None:
            filtre_carburant="Essence"
        if request.POST.get("close_carburant") is not None:
            filtre_carburant=""



    """
    preparation des filtre pour la bdd
    """









    """
    extraction des donnees relative au prix des voitures
    """




    #donnees des voitures
    ##### ensemble des donees
    cars = Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant)

    ##### moyenne des prix des voitures
    prix=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).aggregate(Avg('prix'))

    #creation et preparation des donnees ralative aux transmissions

    #prix


    transmission=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('transmission').annotate(moyenne_transmission=Avg('prix'))
    ##transmission=db.session.query(Car.transmission,func.avg(Car.prix)).filter(Car.localisation.like(payss),Car.carburant.like(filtre_carburant_v),Car.transmission.like(filtre_transmission_v)).group_by(Car.transmission).order_by(func.avg(Car.prix)).all()

    label_transmission=[]
    data_transmission=[]
    for tran in transmission:
        label_transmission.append(tran['transmission'])
        data_transmission.append(round(tran['moyenne_transmission']))

    #nombre

    pour_transmission=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('transmission').annotate(nombre_transmission=Count('prix'))
    label_pour_transmission=[]
    data_pour_transmission=[]
    somme_transmission=0
    for pour_car in pour_transmission:
        somme_transmission+=pour_car['nombre_transmission']

    for pour_car in pour_transmission:
        label_pour_transmission.append(pour_car['transmission'])
        data_pour_transmission.append(round((pour_car['nombre_transmission']/somme_transmission)*100))








    #creation et preparation des donnees ralative aux carburants
    #prix
    carburant=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('carburant_field').annotate(moyenne_carburant=Avg('prix'))
    label_carburant=[]
    data_carburant=[]
    for car in carburant:
        label_carburant.append(car['carburant_field'])
        data_carburant.append(round(car['moyenne_carburant']))

    #Nombre
    pour_carburant=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('carburant_field').annotate(nombre_carburant=Count('prix'))
    label_pour_carburant=[]
    data_pour_carburant=[]
    somme_carburant=0
    for pour_car in pour_carburant:
        somme_carburant+=pour_car['nombre_carburant']

    for pour_car in pour_carburant:
        label_pour_carburant.append(pour_car['carburant_field'])
        data_pour_carburant.append(round((pour_car['nombre_carburant']/somme_carburant)*100))



    #creation et preparation des donnees ralative aux constructeurs

    #########prix
    constructeur=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('constructeur').annotate(moyenne_constructeur=Avg('prix'))
    #########nombre
    pour_constructeur=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('constructeur').annotate(nombre_constructeur=Count('prix')).order_by('-nombre_constructeur')
    Top_constructeur=pour_constructeur[0]
    nb_Top_constructeur=Top_constructeur['nombre_constructeur']
    Top_constructeur=Top_constructeur['constructeur']


    #creation et preparation des donnees ralative aux constructeurs-model
    #########prix
    constructeur_modele=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('constructeur','modele').annotate(moyenne_constructeur=Avg('prix'))
    #########nombre
    pour_constructeur_modele=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('constructeur','modele').annotate(nombre_constructeur_modele=Count('prix')).order_by('-nombre_constructeur_modele')
    Top_constructeur_mode=pour_constructeur_modele[0]
    nb_Top_constructeur_model=Top_constructeur_mode['nombre_constructeur_modele']
    c_Top_constructeur_model=Top_constructeur_mode['constructeur']
    m_Top_constructeur_model= Top_constructeur_mode['modele']




    #creation et preparation des donnees ralative aux vendeurs
    #########prix
    vendeur=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('vendeur_field').annotate(moyenne_vendeur=Avg('prix'))
    #########nombre
    pour_vendeur=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('vendeur_field').annotate(nombre_vendeur=Count('prix')).order_by('-nombre_vendeur')
    Top_vendeur=pour_vendeur[0]
    nb_Top_vendeur=Top_vendeur['nombre_vendeur']
    Top_vendeur=Top_vendeur['vendeur_field']


    #creation et preparation des donnees ralative aux annees
    #########prix
    annee=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('annee').annotate(moyenne_annee=Avg('prix')).order_by('annee')
    label_annee=[]
    data_annee=[]
    for an in annee:
        label_annee.append(an['annee'])
        data_annee.append(an['moyenne_annee'])
    #########nombre
    pour_annee=Cars.objects.filter(localisation__contains=pays, transmission__contains=filtre_transmission, carburant_field__contains=filtre_carburant).values('annee').annotate(nombre_annee=Count('prix'))

    label_pour_annee=[]
    data_pour_annee=[]
    for an in annee:
        label_pour_annee.append(an['annee'])
        #data_pour_annee.append(an['nombre_annee'])























    return render(request,'dashboard/startbootstrap-sb-admin-2-master/index.html', { 'pays':pays,'title':"Dashboard",'annee':annee,'transmission':transmission,'carburant':carburant,'constructeur_modele':constructeur_modele,'constructeur':constructeur,'label_annee':label_annee,'data_annee':data_annee,'label_transmission':label_transmission,'data_transmission':data_transmission,'prix':round(prix['prix__avg']),'label_carburant':label_carburant,'data_carburant':data_carburant,'label_pour_carburant':label_pour_carburant,'data_pour_carburant':data_pour_carburant,'label_pour_transmission':label_pour_transmission,'data_pour_transmission':data_pour_transmission,'nb_Top_constructeur_model':nb_Top_constructeur_model,'Top_constructeur':Top_constructeur,'nb_Top_constructeur':nb_Top_constructeur,'filtre_transmission':filtre_transmission,'filtre_carburant':filtre_carburant,'Top_vendeur':Top_vendeur,'nb_Top_vendeur':nb_Top_vendeur,'cars':cars , 'c_Top_constructeur_model':c_Top_constructeur_model, 'm_Top_constructeur_model':m_Top_constructeur_model})
