from django.apps import AppConfig


class ImmodashboardConfig(AppConfig):
    name = 'immodashboard'
