#Importons les modules nécessaires
from django import forms
from django.contrib.auth.models import User

"""
    Créons le formulaire de connexion
"""
class ConnexionForm(forms.Form):
    username = forms.CharField(label="Username", max_length=50, widget=forms.TextInput(attrs={'type':'text', 'class':'form-control form-control-user', 'id':'exampleInputEmail', 'aria-describedby':'emailHelp', 'placeholder':'Enter Email Address...'}))   
    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'type':'password', 'class':'form-control form-control-user', 'id':'exampleInputPassword',  'placeholder':'Password'}))


"""
    Créons le formulaire d'inscription
"""
class InscriptionForm(forms.Form):
    first_name = forms.CharField(label="First Name", max_length=100, widget=forms.TextInput(attrs={'class' : 'myfieldclass'}))
    last_name = forms.CharField(label="Last Name", max_length=100, widget=forms.TextInput(attrs={'class' : 'myfieldclass'}))
    username = forms.CharField(label="Username", max_length=50, widget=forms.TextInput(attrs={'class' : 'myfieldclass'}))
    email = forms.EmailField(label="Email", widget=forms.TextInput(attrs={'class' : 'myfieldclass'}))   
    password = forms.CharField(label="Password", widget=forms.TextInput(attrs={'class' : 'myfieldclass'}))