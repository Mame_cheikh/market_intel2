#Importons les modules nécessaires
from django.shortcuts import render, redirect
from .models import Immobilier
from .forms import ConnexionForm, InscriptionForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.db.models import Count, Max, Min, Sum


"""
    Créons la vue qui permettra d'afficher dans un tableau toutes les lignes de notre BDD
"""
def dashboard(request):
    offres = Immobilier.objects.all()
    return render(request, 'immodashboard/dashboard.html',{'offres' : offres})

"""
    Créons la vue pour le formulaire de connexion
"""
def connexion(request):
    #Chargeaons le formulaire de connexion
    form = ConnexionForm(request.POST)

    if form.is_valid():
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]

        #Testons la connexion avec les informations de l'utilisateur
        user = authenticate(username = username, password = password)
        if user : 
            login(request, user)
            return redirect(request, 'immodashboard/dashboard.html', locals())
        else:
            error = True
            return render(request, 'immodashboard/connexion.html', locals())
    else:
        form = ConnexionForm()

        return render(request, 'immodashboard/connexion.html', locals())

"""
    Créons la vue pour le formulaire d'Inscription
"""
def inscription(request):
    #Chargeaons le formulaire d'inscription
    form = InscriptionForm(request.POST)

    if form.is_valid():
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        first_name = form.cleaned_data["first_name"]
        last_name = form.cleaned_data["last_name"]
        email = form.cleaned_data["email"]

        #Insérons un nouvel utilisateur dans la BDD à l'aide de la méthode create_user
        user = User.objects.create_user(username = username, email = email, password = password)
         
        user.first_name, user.last_name = first_name, last_name
        user.save()

        #Authentifions le nouvel utilisateur
        user = authenticate(username = username, password = password)
        login(request, user)

        return render(request, 'immodashboard/dashboard.html', locals())
    else:
        form = InscriptionForm()

        return render(request, 'immodashboard/inscription.html', locals())

"""
    Voici la vue permettant d'afficher les données sur le Togo
"""
def togo(request):
    #Extrayons toutes les offres du Togo et regroupons-les selon les villes
    choix = Immobilier.objects.filter(country__contains='Togo')
    offres = choix.values('city').annotate(dcount=Count('city'))

    #Voici les variables que nous utiliserons
    max_price = []
    min_price = []
    datas = []
    labels = []

    #Traitons les informations reçues
    for offre in offres:
        datas.append(offre['dcount'])
        labels.append(offre['city'])
        city = Immobilier.objects.filter(city=offre['city'])
        max = city.aggregate(Max('price'))
        min = city.aggregate(Min('price'))
        max_price.append(max['price__max'])
        min_price.append(min['price__min'])

    
    return render(request, 'immodashboard/togo.html', {'offres':offres,'datas': datas, 'labels':labels, 'min_price': min_price, 'max_price': max_price})

"""
    Voici la vue permettant d'afficher les données sur le Caméroun
"""
def cameroun(request):
    #Extrayons toutes les offres du Caméroun et regroupons-les selon les villes
    choix = Immobilier.objects.filter(country__contains='Cameroun')
    offres = choix.values('city').annotate(dcount=Count('city'))
    
    #Voici les variables que nous utiliserons
    max_price = []
    min_price = []
    datas = []
    labels = []


    #Traitons les informations reçues
    for offre in offres:
        datas.append(offre['dcount'])
        labels.append(offre['city'])
        city = Immobilier.objects.filter(city__contains=offre['city'])
        max = city.aggregate(Max('price'))
        min = city.aggregate(Min('price'))
        max_price.append(max['price__max'])
        min_price.append(min['price__min'])

    
    return render(request, 'immodashboard/cameroun.html', {'offres':offres,'datas': datas, 'labels':labels, 'min_price': min_price, 'max_price': max_price})


"""
    Voici la vue permettant d'afficher les données sur le Burkina
"""
def burkina(request):
    #Extrayons toutes les offres du Burkina et regroupons-les selon les villes
    choix = Immobilier.objects.filter(country__contains='Burkina')
    offres = choix.values('city').annotate(dcount=Count('city'))
    
    #Voici les variables que nous utiliserons
    max_price = []
    min_price = []
    datas = []
    labels = []

    #Traitons les informations reçues
    for offre in offres:
        datas.append(offre['dcount'])
        labels.append(offre['city'])
        city = Immobilier.objects.filter(city__contains=offre['city'])
        max = city.aggregate(Max('price'))
        min = city.aggregate(Min('price'))
        max_price.append(max['price__max'])
        min_price.append(min['price__min'])

    
    return render(request, 'immodashboard/burkina.html', {'offres':offres,'datas': datas, 'labels':labels, 'min_price': min_price, 'max_price': max_price})

"""
    Voici la vue permettant d'afficher les données sur le Bénin
"""
def benin(request):
    #Extrayons toutes les offres du Bénin et regroupons-les selon les villes
    choix = Immobilier.objects.filter(country__contains='Bénin')
    offres = choix.values('city').annotate(dcount=Count('city'))
    
    #Voici les variables que nous utiliserons
    max_price = []
    min_price = []
    datas = []
    labels = []

    #Traitons les informations reçues
    for offre in offres:
        datas.append(offre['dcount'])
        labels.append(offre['city'])
        city = Immobilier.objects.filter(city__contains=offre['city'])
        max = city.aggregate(Max('price'))
        min = city.aggregate(Min('price'))
        max_price.append(max['price__max'])
        min_price.append(min['price__min'])

    
    return render(request, 'immodashboard/benin.html', {'offres':offres,'datas': datas, 'labels':labels, 'min_price': min_price, 'max_price': max_price})


"""
    Voici la vue permettant d'afficher les données sur la Côte d'Ivoire
"""
def cotedivoire(request):
    #Extrayons toutes les offres de la Côte d'Ivoire et regroupons-les selon les villes
    choix = Immobilier.objects.filter(country__contains='Côte')
    offres = choix.values('city').annotate(dcount=Count('city'))
    
    #Voici les variables que nous utiliserons
    max_price = []
    min_price = []
    datas = []
    labels = []

    #Traitons les informations reçues
    for offre in offres:
        #print(offre)
        datas.append(offre['dcount'])
        labels.append(offre['city'])
        city = Immobilier.objects.filter(city__contains=offre['city'])
        max = city.aggregate(Max('price'))
        min = city.aggregate(Min('price'))
        max_price.append(max['price__max'])
        min_price.append(min['price__min'])

    
    return render(request, 'immodashboard/cotedivoire.html', {'offres':offres,'datas': datas, 'labels':labels, 'min_price': min_price, 'max_price': max_price})

"""
    Voici la vue permettant d'afficher les données sur la Tunisie
"""
def tunisie(request):
    #Extrayons toutes les offres de la Tunisie et regroupons-les selon les villes
    choix = Immobilier.objects.filter(country__contains='Tunisie')
    offres = choix.values('city').annotate(dcount=Count('city'), somme=Sum('price'))

    #Voici les variables que nous utiliserons
    max_price = []
    min_price = []
    sums = []
    average = {}
    avg_labels = []
    avg_price = [] 
    datas = []
    labels = []

    #Traitons les informations reçues
    for offre in offres:
        if offre['somme'] is not None:
            datas.append(offre['dcount'])
            labels.append(offre['city'])
            city = choix.filter(city__contains=offre['city'])
            max = city.aggregate(Max('price'))
            min = city.aggregate(Min('price'))
            max_price.append(max['price__max'])
            min_price.append(min['price__min'])
            sums.append(offre['somme'])
            average[offre['city']] = offre['somme']//offre['dcount']

    #On trie par ordre croissant la moyenne des prix par ville
    sort_average = sorted(average.items(), key=lambda x: x[1])
    for avg in sort_average:
        avg_labels.append(avg[0])
        avg_price.append(avg[1])
    
    #Pour avoir les prix Max et Min
    max = choix.aggregate(max=Max('price'))
    min = choix.aggregate(min=Min('price'))
    max = choix.filter(price=max['max']) 
    min = choix.filter(price=min['min'])
    
    return render(request, 'immodashboard/tunisie.html', {'offres':offres,'datas': datas, 'labels':labels, 'min_price': min_price, 'max_price': max_price, 'sums':sums, 'average': avg_price,'avg_labels': avg_labels, 'max': max[0], 'min': min[0]})

"""
    Voici la vue permettant d'afficher les données sur l'Algérie
"""
def algerie(request):
    #Extrayons toutes les offres de l'Algérie et regroupons-les selon les villes
    choix = Immobilier.objects.filter(country__contains='Algérie')
    offres = choix.values('city').annotate(dcount=Count('city'), somme=Sum('price'))
    
    #Voici les variables que nous utiliserons
    max_price = []
    min_price = []
    sums = []
    average = {}
    avg_labels = []
    avg_price = [] 
    datas = []
    labels = []

    #Traitons les informations reçues
    for offre in offres:
        if offre['somme'] is not None:
            datas.append(offre['dcount'])
            labels.append(offre['city'])
            city = choix.filter(city__contains=offre['city'])
            max = city.aggregate(Max('price'))
            min = city.aggregate(Min('price'))
            max_price.append(max['price__max'])
            min_price.append(min['price__min'])
            sums.append(offre['somme'])
            average[offre['city']] = offre['somme']//offre['dcount']

    #On trie par ordre croissant la moyenne des prix par ville
    sort_average = sorted(average.items(), key=lambda x: x[1])
    for avg in sort_average:
        avg_labels.append(avg[0])
        avg_price.append(avg[1])

    #Pour avoir les prix Max et Min
    max = choix.aggregate(max=Max('price'))
    min = choix.aggregate(min=Min('price'))
    max = choix.filter(price=max['max']) 
    min = choix.filter(price=min['min'])
    
    return render(request, 'immodashboard/algerie.html', {'offres':offres,'datas': datas, 'labels':labels, 'min_price': min_price, 'max_price': max_price, 'sums':sums, 'average': avg_price,'avg_labels': avg_labels, 'max': max[0], 'min': min[0]})


"""
    Voici la vue permettant d'afficher les données sur le Maroc
"""
def maroc(request):
    #Extrayons toutes les offres de la Maroc et regroupons-les selon les villes
    choix = Immobilier.objects.filter(country__contains='Maroc')
    offres = choix.values('city').annotate(dcount=Count('city'), somme=Sum('price'))
    
    #Voici les variables que nous utiliserons
    max_price = []
    min_price = []
    sums = []
    average = {}
    avg_labels = []
    avg_price = [] 
    datas = []
    labels = []

    #Traitons les informations reçues
    for offre in offres:
        if offre['somme'] is not None:
            datas.append(offre['dcount'])
            labels.append(offre['city'])
            city = choix.filter(city__contains=offre['city'])
            max = city.aggregate(Max('price'))
            min = city.aggregate(Min('price'))
            max_price.append(max['price__max'])
            min_price.append(min['price__min'])
            sums.append(offre['somme'])
            average[offre['city']] = offre['somme']//offre['dcount']

    #On trie par ordre croissant la moyenne des prix par ville
    sort_average = sorted(average.items(), key=lambda x: x[1])
    for avg in sort_average:
        avg_labels.append(avg[0])
        avg_price.append(avg[1])
    
    #Pour avoir les prix Max et Min
    max = choix.aggregate(max=Max('price'))
    min = choix.aggregate(min=Min('price'))
    max = choix.filter(price=max['max']) 
    min = choix.filter(price=min['min'])
    
    return render(request, 'immodashboard/maroc.html', {'offres':offres,'datas': datas, 'labels':labels, 'min_price': min_price, 'max_price': max_price, 'sums':sums, 'average': avg_price,'avg_labels': avg_labels, 'max': max[0], 'min': min[0]})

