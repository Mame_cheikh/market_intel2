#Importons les modules nécessaires
from django.urls import path
from . import views

#Définissons les routes vers nos vues
urlpatterns = [
    path('dashboard', views.dashboard, name = "dashboard"),
    path('', views.connexion, name = "connexion"),
    path('inscription', views.inscription, name = "inscription"),
    path('togo', views.togo, name = "togo"),
    path('cameroun', views.cameroun, name = "cameroun"),
    path('tunisie', views.tunisie, name = "tunisie"),
    path('algerie', views.algerie, name = "algerie"),
    path('maroc', views.maroc, name = "maroc"),
    path('burkina', views.burkina, name = "burkina"),
    path('benin', views.benin, name = "benin"),
    path('cotedivoire', views.cotedivoire, name = "cotedivoire"),
]
