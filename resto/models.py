from django.conf import settings
from django.db import models
from django.utils import timezone



class restaurants(models.Model):
    __tablename__ = 'restaurants'
  
    id =models.IntegerField(primary_key=True)
    nom = models.TextField()
    description = models.TextField()
    adresse=models.TextField()
    pays=models.TextField()
    telephone=models.TextField()
    region=models.TextField()

    def __repr__(self):
        return '<Restaurants: {}>'.format(self.nom)