from django.http import HttpResponseRedirect,HttpResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views import generic

from .models import restaurants
from .forms import  chercher


def acceuil(request):
    return render(request, 'home/index.html')



def capitale(request):
    Dakar = restaurants.objects.filter(region='Dakar').count()
    Ouagadougou = restaurants.objects.filter(region='Ouagadougou').count()
    Lome= restaurants.objects.filter(region="Lomé").count()
    Abidjan = restaurants.objects.filter(region='Abidjan').count()
    Cotonou = restaurants.objects.filter(region='Cotonou').count()
    nombres=[Dakar,Lome,Ouagadougou,Abidjan,Cotonou]
    context={'nombres':nombres}

    return render(request, 'home/capitale.html',context)


def donnee(request):
    Dakar=restaurants.objects.filter(region='Dakar').count()
    Mbour=restaurants.objects.filter(region='M\'Bour').count()
    Saint_Louis=restaurants.objects.filter(region='Saint-Louis').count()
    senegal=[Dakar,Saint_Louis,Mbour]

    Abomey = restaurants.objects.filter(region='Abomey-Calavi').count()
    Bohicon = restaurants.objects.filter(region='Bohicon').count()
    Porto = restaurants.objects.filter(region='Porto-Novo').count()
    Cotonou = restaurants.objects.filter(region='Cotonou').count()
   
    benin=[Cotonou,Abomey,Bohicon,Porto]
    Grand=restaurants.objects.filter(region='Grand-Bassam').count()
    Yamoussoukro=restaurants.objects.filter(region='Yamoussoukro').count()
    Abidjan = restaurants.objects.filter(region='Abidjan').count()
   
    cote=[Abidjan,Yamoussoukro,Grand]
    
    Koudougou=restaurants.objects.filter(region='Koudougou').count()
    Bobo=restaurants.objects.filter(region='Bobo-Dioulasso').count()
    ouagadougou=restaurants.objects.filter(region='Ouagadougou').count()
    burkina=[ouagadougou,Bobo,Koudougou]

    
    lome=restaurants.objects.filter(region='Lomé').count()
    Atakpame=restaurants.objects.filter(region='Atakpamé').count()
    Kpalime=restaurants.objects.filter(region='Kpalimé').count()
    togo=[lome,Atakpame,Kpalime]



    context = {'senegal': senegal,'benin': benin,'cote':cote,'burkina':burkina,'togo':togo}
    
    return render(request, 'home/statistique.html',context)


def recherche(request):
    restaurant=None
    
    form = chercher(request.GET)
    if request.method == "GET":
        if form.is_valid():
            search=''
            pays=''
            search=request.GET.get('recherche', None)
            pays=request.GET.get('pays', None)
            restaurant=restaurants.objects.filter(nom__icontains=search,pays=pays).all()
            if pays=='0' :
                restaurant=restaurants.objects.filter(nom__icontains=search).all()


    context= {'restaurant': restaurant,'form': form}
    return render(request, 'home/recherche.html',context )

def regarder(request, id):
    r = get_object_or_404(restaurants, id=id)
    restaurant=None
    form = chercher(request.GET)
    if request.method == "GET":
            search=''
            pays=''
            search=request.GET.get('recherche', None)
            if search==None :
                search=''
            pays=request.GET.get('pays', None)
            if pays==None :
                pays=''
            restaurant=restaurants.objects.filter(nom__icontains=search,pays=pays).all()
            if pays=='0' :
                restaurant=restaurants.objects.filter(nom__icontains=search).all()
    
    context= {'restaurant': restaurant,'form': form,'r':r}
    return render(request, 'home/recherche.html', context)





