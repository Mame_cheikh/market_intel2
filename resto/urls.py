
from django.urls import path 
from . import views

urlpatterns = [
    path('accueil/', views.acceuil, name='index'),
    path('statistiques/', views.donnee, name='statistique'),
    path('capitales/', views.capitale, name='capitales'),
    path('recherche/', views.recherche, name='recherche'),
    path('recherche/<int:id>/', views.regarder, name='regarder'),
    
]